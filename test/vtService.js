const vt = require('vasttrafik-api');
const config = require('./config');


const fetchDeparture = async () => {
    const dateObject = new Date();
    const date = dateObject.toLocaleDateString();
    const time = dateObject.toLocaleTimeString();
    const options = {'timeSpan': 30,'maxDeparturesPerLine': 2}

    await vt.authorize(config.key, config.secret);
    let api = new vt.DepartureBoardApi();
    const res = await api.getDepartureBoard(config.stationId, date, time, options);
    return (res.body);
}

const fetchLocationId = async (locationName) => {
    await vt.authorize(config.key, config.secret);
    let api = new vt.LocationApi();
    const res = await api.getLocationByName({input: locationName});
    return (res.body.LocationList.StopLocation.shift());
}

const fetchFixedJourney = async (locationStop) => {
    const dateObject = new Date();
    const date = dateObject.toLocaleDateString();
    const time = dateObject.toLocaleTimeString();
    const options = {'timeSpan': 60,'maxDeparturesPerLine': 2, 'direction': locationStop.id}

    await vt.authorize(config.key, config.secret);
    let api = new vt.DepartureBoardApi();
    const res = await api.getDepartureBoard(config.stationId, date, time, options);
    return (res.body);
}

const fetchDepartureBoard = async (stopId) => {
    const dateObject = new Date();
    const date = dateObject.toLocaleDateString();
    const time = dateObject.toLocaleTimeString();
    const options = {'timeSpan': 30,'maxDeparturesPerLine': 2}

    await vt.authorize(config.key, config.secret);
    let api = new vt.DepartureBoardApi();
    const res = await api.getDepartureBoard(stopId, date, time, options);
    return (res.body);
}

module.exports.fetchDeparture = fetchDeparture;
module.exports.fetchLocationId = fetchLocationId;
module.exports.fetchFixedJourney = fetchFixedJourney;
module.exports.fetchDepartureBoard = fetchDepartureBoard;
