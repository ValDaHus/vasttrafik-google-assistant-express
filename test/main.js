const express = require('express')
const bodyParser = require('body-parser');  
const { dialogflow, Table, Image } = require('actions-on-google');
const vtService = require('./vtService')
const messageHelper = require('./messageHelper')

const df = dialogflow({
    debug: true
});

df.intent('nextdeparture', async (conv, params) => {
  let data = await vtService.fetchDeparture();
  const line = (params.number || params.color.toUpperCase())
  const message = messageHelper.createNextDepartureMessage(data, line)

  conv.close(message);
});

df.intent('fixedjourney', async (conv, params) => {
  let station = await vtService.fetchLocationId(params.any);
  let data = await vtService.fetchFixedJourney(station);
  const message = messageHelper.createJourneyMessage(data);

  conv.close(message);
});

df.intent('dashboard', async (conv, params) => {
  let station = await vtService.fetchLocationId(params.any);
  let data = await vtService.fetchDepartureBoard(station.id);
  const rows = messageHelper.createTableRows(data);

  conv.ask('Tidtabellen för hållplatsen: ' + station.name);
  conv.close(new Table({
    title: station.name,
    subtitle: new Date().toLocaleTimeString('sv-SE', { hour: "numeric", minute: "numeric"}),
    image: new Image({
      url: 'https://storage.googleapis.com/actionsresources/logo_assistant_2x_64dp.png',
      alt: 'Image alternate text',
    }),
    columns: [
      { header: 'Linje', align: 'LEADING' },
      { header: 'Nästa', align: 'CENTER' },
      { header: 'Läge', align: 'TRAILING' },
    ],
    rows: rows
  }));
});

const app = express().use(bodyParser.json());
app.post('/', df);

app.get('/ping', async (req, res) => {
  res.send('pong');
})

app.listen(3000, () => console.log(`App started on port 3000!`));
