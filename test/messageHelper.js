const config = require('./config');

const createNextDepartureMessage = (data, sname) => {
    let departure = data.DepartureBoard.Departure
        .filter(departure => departure.stopid === config.stationId)
        .find(departure => departure.sname === sname);

    const message = departure ? `${departure.name} åker klockan ${departure.rtTime}` : 'Avgång saknas';
    return message;
}

const createJourneyMessage = (data) => {
    const [firstDeparture, secondDeparture] = data.DepartureBoard.Departure.slice(0,2);
    const message = firstDeparture ? 
        `${firstDeparture.name} mot ${firstDeparture.direction} åker klockan ${firstDeparture.rtTime}, ` 
        + `därefter åker ${secondDeparture.name} mot ${secondDeparture.direction} klockan ${secondDeparture.rtTime}` 
        : 'Avgång saknas';
    return message;
}

const createTableRows = (data) => {
    let rows = [];    
    data.DepartureBoard.Departure.forEach(departue => {
        rows.push({
            cells: [`${departue.name} mot ${departue.direction}` , departue.rtTime, departue.track],
            dividerAfter: true,
        })
    })
    return rows
}

module.exports.createNextDepartureMessage = createNextDepartureMessage;
module.exports.createJourneyMessage = createJourneyMessage;
module.exports.createTableRows = createTableRows;
